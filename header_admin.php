

<?PHP

session_start();

// if (!isset($_SESSION['id']) && $_SESSION['id'] == false) {
//     header("Location: http://www.hackatom.com.br/login.php");
// }

// Arquivo editado para teste.

	require_once("includes/session.php");
	if(checkSession()){
	    header("Location: http://www.hackatom.com.br/login.php");
	    exit;
	}
?>




<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="HackAtom" />
	<meta name="description" content="Como o Hackathon pode transformar seu negócio.">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet prefetch' href='http://www.tinymce.com/css/codepen.min.css'>
	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/calendar.css" type="text/css" />
	<link rel="stylesheet" href="css/admin.css" type="text/css" />
  <link rel="stylesheet" href="css/login.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- Document Title
	============================================= -->
	<title>HackAtom</title>
</head>

<body class="stretched">
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header
		============================================= -->
		<header id="header" class="full-header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div><!-- #logo end -->
				</div>
			</div>
		</header><!-- #header end -->

		<div id="vertical-nav">
			<div class="container clearfix">
        <!-- MENU VERTICAL ADMIN -->
				<nav>
					<ul>
                <li>
                    <?php // exibe o nome de quem está logado.
                        if (isset($_SESSION['id'])) {
                                $id = $_SESSION['id'];
                                //echo $id;
                                global $connection;
                                $query = "SELECT nome FROM t_usuario WHERE ID_USUARIO=$id";
                                $resultado = mysqli_query($connection, $query);
                                      if ($resultado) {
                                            $row = mysqli_fetch_assoc($resultado);
                                            echo "<h4>  Bem-vindo! " .  $row['nome'] . "</h4>";
                                          }// fecha IF row
                        }// fecha IF principal
                    ?>
                </li>

						<li><a href="#"><i class="icon-lightbulb"></i>Posts</a>
							<ul>
								<li><a href="post_new.php"><i class="icon-line-paper"></i>Novo Post</a></li>
								<li><a href="post_edit.php"><i class="icon-pencil"></i>Editar Post</a></li>
							</ul>
						</li>

						<li><a href="#"><i class="icon-line-head"></i>Usuários</a>
							<ul>
								<li><a href="#">Lista usuários</a></li>
								<li><a href="#">Lista de E-mails</a></li>
							</ul>
						</li>

            <br><br>
            <!-- BOTÃO QUE FAZ O LOGOUT -->
						<li><a href="logout.php"><i class="icon-line2-logout"></i>Logout</a></li>
          </ul>
				</nav>
        <!-- FECHA MENU VERTICAL ADMIN  -->
			</div>
		</div>

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Content Management</h1>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
